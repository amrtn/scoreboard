package es.dummy;

import es.dummy.domain.Game;
import es.dummy.domain.Scoreboard;
import es.dummy.domain.exceptions.GameNotFoundException;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class ScoreboardTest {
    @Test
    public void should_GameHasInitialScore_When_GameJustStarted() {
        Scoreboard scb = new Scoreboard();
        scb.addGame("Uruguay", "Italy");

        Game game = scb.getSummary().get(0);

        Assert.assertEquals("The home team initial score is not 0", 0, game.getHomeScore());
        Assert.assertEquals("The away team initial score is not 0", 0, game.getAwayScore());
    }

    @Test
    public void should_ChangeHomeScoreInGame_When_GameUpdated() throws Exception {
        Scoreboard scb = new Scoreboard();
        scb.addGame("Uruguay", "Italy");
        scb.updateGame("Uruguay", "Italy", 1, 0);
        Assert.assertEquals("The home score has not changed or the value is unexpected",
                1, scb.getSummary().get(0).getHomeScore());
    }

    @Test(expected = GameNotFoundException.class)
    public void should_Throw_When_AttemptedToUpdateAGameNotYetInTheScoreboard() throws Exception {
        Scoreboard scb = new Scoreboard();
        scb.updateGame("Uruguay", "Italy", 1, 0); // Should throw here
    }

    @Test
    public void should_GenerateSummary_When_OneGameIsInTheScoreboard() {
        Scoreboard scb = new Scoreboard();
        scb.addGame("Uruguay", "Italy");
        List<Game> summary = scb.getSummary();
        Assert.assertEquals("The number of games in the summary is wrong", 1, summary.size());
        Game firstGame = summary.get(0);
        Assert.assertTrue("The added game is not in the summary",
                "Uruguay".equals(firstGame.getHomeTeam()) && "Italy".equals(firstGame.getAwayTeam()));
    }

    @Test
    public void should_GenerateSummary_When_OneGameWithScoreIsInTheScoreboard() throws GameNotFoundException {
        Scoreboard scb = new Scoreboard();
        scb.addGame("Uruguay", "Italy");
        scb.updateGame("Uruguay", "Italy", 2, 1);
        List<Game> summary = scb.getSummary();
        Assert.assertEquals("The modified home score in the summary is wrong", 2, summary.get(0).getHomeScore());
        Assert.assertEquals("The modified away score in the summary is wrong", 1, summary.get(0).getAwayScore());
    }

    @Test
    public void should_SortSummaryByTotalScore_When_MultipleGamesWithDifferentScores() throws GameNotFoundException {
        Scoreboard scb = new Scoreboard();

        scb.addGame("Mexico", "Canada");
        scb.addGame("Germany", "France");
        scb.addGame("Uruguay", "Italy");

        scb.updateGame("Mexico", "Canada", 0, 5);
        scb.updateGame("Germany", "France", 2, 2);
        scb.updateGame("Uruguay", "Italy", 6, 6);

        List<Game> summary = scb.getSummary();
        Assert.assertEquals("The number of games in the summary is wrong", 3, summary.size());
        Game firstGame = summary.get(0);
        Game secondGame = summary.get(1);
        Game thirdGame = summary.get(2);

        // First game: Uruguay - Italy
        Assert.assertTrue("The first game in the summary is not Uruguay-Italy",
                "Uruguay".equals(firstGame.getHomeTeam()) && "Italy".equals(firstGame.getAwayTeam()));
        Assert.assertTrue("The first game has the wrong score",
                firstGame.getHomeScore() == 6 && firstGame.getAwayScore() == 6);

        // Second game: Mexico - Canada
        Assert.assertTrue("The second game in the summary is not Mexico-Canada",
                "Mexico".equals(secondGame.getHomeTeam()) && "Canada".equals(secondGame.getAwayTeam()));
        Assert.assertTrue("The second game has the wrong score",
                secondGame.getHomeScore() == 0 && secondGame.getAwayScore() == 5);


        // Third game: Germany - France
        Assert.assertTrue("The third game in the summary is not Germany-France",
                "Germany".equals(thirdGame.getHomeTeam()) && "France".equals(thirdGame.getAwayTeam()));
        Assert.assertTrue("The third game has the wrong score",
                thirdGame.getHomeScore() == 2 && thirdGame.getAwayScore() == 2);
    }

    @Test
    public void should_UpdateTheScoreBoard_When_ReceivedSuccessiveUpdatesOfAGame() throws GameNotFoundException {
        Scoreboard scb = new Scoreboard();

        scb.addGame("Mexico", "Canada");
        scb.addGame("Germany", "France");
        scb.addGame("Uruguay", "Italy");

        scb.updateGame("Mexico", "Canada", 0, 0);
        scb.updateGame("Germany", "France", 2, 2);
        scb.updateGame("Mexico", "Canada", 0, 1);
        scb.updateGame("Uruguay", "Italy", 6, 6);
        scb.updateGame("Mexico", "Canada", 0, 2);
        scb.updateGame("Mexico", "Canada", 0, 3);
        scb.updateGame("Mexico", "Canada", 0, 4);
        scb.updateGame("Mexico", "Canada", 0, 5);

        List<Game> summary = scb.getSummary();
        Assert.assertEquals("The number of games in the summary is wrong", 3, summary.size());
        Game firstGame = summary.get(0);
        Game secondGame = summary.get(1);
        Game thirdGame = summary.get(2);

        // First game: Uruguay - Italy
        Assert.assertTrue("The first game in the summary is not Uruguay-Italy",
                "Uruguay".equals(firstGame.getHomeTeam()) && "Italy".equals(firstGame.getAwayTeam()));
        Assert.assertTrue("The first game has the wrong score",
                firstGame.getHomeScore() == 6 && firstGame.getAwayScore() == 6);

        // Second game: Mexico - Canada
        Assert.assertTrue("The second game in the summary is not Mexico-Canada",
                "Mexico".equals(secondGame.getHomeTeam()) && "Canada".equals(secondGame.getAwayTeam()));
        Assert.assertTrue("The second game has the wrong score",
                secondGame.getHomeScore() == 0 && secondGame.getAwayScore() == 5);


        // Third game: Germany - France
        Assert.assertTrue("The third game in the summary is not Germany-France",
                "Germany".equals(thirdGame.getHomeTeam()) && "France".equals(thirdGame.getAwayTeam()));
        Assert.assertTrue("The third game has the wrong score",
                thirdGame.getHomeScore() == 2 && thirdGame.getAwayScore() == 2);
    }

    @Test
    public void should_SortSummaryByTotalScoreAndInsertionOrder_When_MultipleGamesWithEqualScores() throws GameNotFoundException {
        Scoreboard scb = new Scoreboard();

        scb.addGame("Mexico", "Canada");
        scb.addGame("Spain", "Brazil");
        scb.addGame("Germany", "France");
        scb.addGame("Uruguay", "Italy");
        scb.addGame("Argentina", "Australia");

        scb.updateGame("Spain", "Brazil", 5, 2);
        scb.updateGame("Mexico", "Canada", 0, 5);
        scb.updateGame("Germany", "France", 2, 2);
        scb.updateGame("Uruguay", "Italy", 6, 6);
        scb.updateGame("Spain", "Brazil", 10,2);
        scb.updateGame("Argentina", "Australia", 1, 0);
        scb.updateGame("Argentina", "Australia", 3,1);

        List<Game> summary = scb.getSummary();
        Assert.assertEquals("The number of games in the summary is wrong", 5, summary.size());
        Game firstGame = summary.get(0);
        Game secondGame = summary.get(1);
        Game thirdGame = summary.get(2);
        Game fourthGame = summary.get(3);
        Game fifthGame = summary.get(4);

        // First game: Uruguay - Italy
        Assert.assertTrue("The first game in the summary is not Uruguay-Italy",
                "Uruguay".equals(firstGame.getHomeTeam()) && "Italy".equals(firstGame.getAwayTeam()));
        Assert.assertTrue("The first game has the wrong score",
                firstGame.getHomeScore() == 6 && firstGame.getAwayScore() == 6);

        // Second game: Spain - Brazil
        Assert.assertTrue("The second game in the summary is not Spain-Brazil",
                "Spain".equals(secondGame.getHomeTeam()) && "Brazil".equals(secondGame.getAwayTeam()));
        Assert.assertTrue("The second game has the wrong score",
                secondGame.getHomeScore() == 10 && secondGame.getAwayScore() == 2);

        // Third game: Mexico - Canada
        Assert.assertTrue("The third game in the summary is not Mexico-Canada",
                "Mexico".equals(thirdGame.getHomeTeam()) && "Canada".equals(thirdGame.getAwayTeam()));
        Assert.assertTrue("The third game has the wrong score",
                thirdGame.getHomeScore() == 0 && thirdGame.getAwayScore() == 5);

        // Fourth game: Argentina - Australia
        Assert.assertTrue("The fourth game in the summary is not Argentina-Australia",
                "Argentina".equals(fourthGame.getHomeTeam()) && "Australia".equals(fourthGame.getAwayTeam()));
        Assert.assertTrue("The fourth game has the wrong score",
                fourthGame.getHomeScore() == 3 && fourthGame.getAwayScore() == 1);

        // Fifth game: Germany - France
        Assert.assertTrue("The fifth game in the summary is not Germany-France",
                "Germany".equals(fifthGame.getHomeTeam()) && "France".equals(fifthGame.getAwayTeam()));
        Assert.assertTrue("The fifth game has the wrong score",
                fifthGame.getHomeScore() == 2 && fifthGame.getAwayScore() == 2);
    }

    @Test(expected = GameNotFoundException.class)
    public void should_Throw_When_AttemptToRemoveANonExistentGame() throws GameNotFoundException {
        Scoreboard scb = new Scoreboard();

        scb.addGame("Mexico", "Canada");
        scb.addGame("Spain", "Brazil");

        scb.removeGame("Uruguay", "Italy");
    }

    @Test
    public void should_RemoveAGameFromTheSummary_When_AGameIsRemovedFromTheScoreboard() throws GameNotFoundException {
        Scoreboard scb = new Scoreboard();

        scb.addGame("Mexico", "Canada");
        scb.addGame("Spain", "Brazil");
        scb.addGame("Germany", "France");
        scb.addGame("Uruguay", "Italy");
        scb.addGame("Argentina", "Australia");

        scb.updateGame("Spain", "Brazil", 5, 2);
        scb.updateGame("Mexico", "Canada", 0, 5);
        scb.updateGame("Germany", "France", 2, 2);
        scb.updateGame("Uruguay", "Italy", 6, 6);
        scb.updateGame("Spain", "Brazil", 10,2);
        scb.updateGame("Argentina", "Australia", 1, 0);
        scb.updateGame("Argentina", "Australia", 3,1);

        scb.removeGame("Uruguay", "Italy");

        List<Game> summary = scb.getSummary();
        Assert.assertEquals("The number of games in the summary is wrong", 4, summary.size());
        Game firstGame = summary.get(0);
        Game secondGame = summary.get(1);
        Game thirdGame = summary.get(2);
        Game fourthGame = summary.get(3);


        // First game: Spain - Brazil
        Assert.assertTrue("The first game in the summary is not Spain-Brazil",
                "Spain".equals(firstGame.getHomeTeam()) && "Brazil".equals(firstGame.getAwayTeam()));
        Assert.assertTrue("The first game has the wrong score",
                firstGame.getHomeScore() == 10 && firstGame.getAwayScore() == 2);

        // Second game: Mexico - Canada
        Assert.assertTrue("The second game in the summary is not Mexico-Canada",
                "Mexico".equals(secondGame.getHomeTeam()) && "Canada".equals(secondGame.getAwayTeam()));
        Assert.assertTrue("The second game has the wrong score",
                secondGame.getHomeScore() == 0 && secondGame.getAwayScore() == 5);

        // Third game: Argentina - Australia
        Assert.assertTrue("The third game in the summary is not Argentina-Australia",
                "Argentina".equals(thirdGame.getHomeTeam()) && "Australia".equals(thirdGame.getAwayTeam()));
        Assert.assertTrue("The third game has the wrong score",
                thirdGame.getHomeScore() == 3 && thirdGame.getAwayScore() == 1);

        // Fourth game: Germany - France
        Assert.assertTrue("The fourth game in the summary is not Germany-France",
                "Germany".equals(fourthGame.getHomeTeam()) && "France".equals(fourthGame.getAwayTeam()));
        Assert.assertTrue("The fourth game has the wrong score",
                fourthGame.getHomeScore() == 2 && fourthGame.getAwayScore() == 2);
    }

}
