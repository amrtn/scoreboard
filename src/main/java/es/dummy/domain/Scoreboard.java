package es.dummy.domain;

import es.dummy.domain.exceptions.GameNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.function.Predicate;

public class Scoreboard {
    private final TreeSet<Game> sortedGames = new TreeSet<>(new GameComparator());


    private int nextIndex = 0;

    public void addGame(String homeTeam, String awayTeam) {
        Game game = new Game(homeTeam, awayTeam, nextIndex++);
        this.sortedGames.add(game);
    }

    public void updateGame(String homeTeam, String awayTeam, int homeScore,
                           int awayScore) throws GameNotFoundException {

        Game game = this.findGame(homeTeam, awayTeam);
        if (game == null) {
            throw new GameNotFoundException();
        }

        // Attention:
        // Since the game is immutable we need to remove it first from the tree before changing its score
        this.sortedGames.remove(game);
        Game updatedGame = new Game(game, homeScore, awayScore);
        this.sortedGames.add(updatedGame);
    }

    public void removeGame(String homeTeam, String awayTeam) throws GameNotFoundException {
        Game game = this.findGame(homeTeam, awayTeam);
        if (game == null) {
            throw new GameNotFoundException();
        }
        this.sortedGames.remove(game);
    }

    /**
     * Returns the summary of live games
     *
     * @return
     */
    public List<Game> getSummary() {
        return new ArrayList<>(this.sortedGames);
    }

    private Game findGame(String homeTeam, String awayTeam) {
        Predicate<Game> gameMatcher =
                (Game game) -> homeTeam.equalsIgnoreCase(game.getHomeTeam()) && awayTeam.equalsIgnoreCase(game.getAwayTeam());
        return this.sortedGames
                .stream()
                .filter(gameMatcher)
                .findFirst().orElse(null);
    }

}
