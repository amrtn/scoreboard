package es.dummy.domain;

public class Game {

    private final String homeTeam;
    private final String awayTeam;

    private final int homeScore;
    private final int awayScore;

    private final int scoreboardIndex;

    public Game(String homeTeam, String awayTeam, int scoreboardIndex) {
        this.awayTeam = awayTeam;
        this.homeTeam = homeTeam;

        this.homeScore = 0;
        this.awayScore = 0;

        this.scoreboardIndex = scoreboardIndex;
    }

    public Game(Game otherGame, int homeScore, int awayScore) {
        this.homeTeam = otherGame.getHomeTeam();
        this.awayTeam = otherGame.getAwayTeam();
        this.scoreboardIndex = otherGame.getScoreboardIndex();
        this.homeScore = homeScore;
        this.awayScore = awayScore;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public String getAwayTeam() {
        return awayTeam;
    }

    public int getHomeScore() {
        return this.homeScore;
    }

    public int getAwayScore() {
        return this.awayScore;
    }

    public int getScoreboardIndex() {
        return scoreboardIndex;
    }

    //<editor-fold desc="Equality">
    /**
     * Any two Game objects with identical home and away teams (case insensitive) are considered to represent the
     * same entity
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Game)) return false;

        Game game = (Game) o;

        if (!getHomeTeam().equalsIgnoreCase(game.getHomeTeam())) return false;
        return getAwayTeam().equalsIgnoreCase(game.getAwayTeam());
    }

    /**
     * Any two Game objects with identical home and away teams (case insensitive) are considered to represent the
     *      * same entity
     * @return
     */
    @Override
    public int hashCode() {
        int result = getHomeTeam().toLowerCase().hashCode();
        result = 31 * result + getAwayTeam().toLowerCase().hashCode();
        return result;
    }
    //</editor-fold>

}
