# Football world cup score board

## About

Author: Álvaro Martín Oliva <alvaro@rodadas.net>


## Assumptions

* The summary operation is going to be invoked very frequently, so we should optimize for fast game retrieval instead
  of fast insertion or update.
 
* Add, update and remove operations are going to be invoked less frequently.

* We opt to keep the internal data structure sorted and ready to be used by the summary operation. That way, while the
  insertion, removal and update operations are costlier, the retrieval operation (which is the most frequently invoked)
  will be resolved in linear time (O(n)). The chosen data structure for the game list is a `SortedSet` of games. In
  order to keep the set sorted we must make the `Game` class implement the `Comparable` interface. Also, there is a
  business rule that dictates which game should come first in the summary when two games have the same total score. In
  this case the game which was added first to the scoreboard should take precedence. To support that rule, we define
  an additional attribute representing the order of insertion of the game into the scoreboard. The scoreboard is
  responsible to set the value of this attribute when the user adds the game to the scoreboard for the first time. The
  `Comparable` implementation also uses this attribute to decide which game should be shown first.

* In order to keep the model as simple as possible, the `Game` class encode the home and away teams as simple strings.
  To ensure the consistency of the scoreboard data structure, the `Game` class is immutable.

* We understand that two `Game` objects conceptually represent the same game if they have the same home and away team
 (case-insensitive). In order to enforce this rule we override the equality and identity methods in the `Game` class.
 This also prevents to insert the same game twice in the scoreboard.
 
* The public API of the `Scoreboard` object accepts only primitive types, since that is the way a hypothetical REST API
  would receive the data. The `Game` object is only visible as a part of the summary.

## Notes

The project uses Java 11 and is a simple commandline application initialized with the maven archetype 
`maven-archetype-quickstart`.

I tend to use prefixes for my commit messages: 

* `Add`: Add a feature
* `Clean`: Clean or reformat code
* `Refactor`: Refactor code
* `Doc`: Improve documentation
* `Test`: Add or update tests
* `Fix`: Correct an issue or problem

## Build

```bash
mvn clean install
```

## Test

```bash
mvn test
```